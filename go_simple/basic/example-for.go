package main

import (
	"bufio"
	"fmt"
	"os"
)

func sum() int {
	var d int
	for i := 0; i < 100; i++ {
		d++
	}
	return d
}

func printFile() {
	file, err := os.Open("D:/go_study/go_simple/basic/123.txt")
	// 如果出错，结束进程
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	// 获取读取器
	scanner := bufio.NewScanner(file)
	// 读取：It returns false when the scan stops, either by reaching the end of the input or an error
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

// 等同于 while(true)
func deepLoop() {
	var i = 0
	for {
		i++
		fmt.Println(i)
		if i == 3 {
			fmt.Println("break")
			break
		}
	}
}

func main() {
	fmt.Println(sum())
	printFile()
	// while true
	deepLoop()
}
