package main

import (
	"errors"
	"fmt"
)

func checkAge(age int) error {
	if age < 0 {
		return fmt.Errorf("age是：%d，年龄不能为负数", age)
	}
	fmt.Println("年龄无误: ", age)
	return nil
}

func test() (int, int, error) {
	return 1, 2, errors.New("错误啦，啦啦啦")
}

func main() {
	var e1 error
	fmt.Println(e1)        // <nil>
	fmt.Printf("%T\n", e1) // <nil>

	e2 := errors.New("错误了")
	fmt.Println(e2)        // 错误了
	fmt.Printf("%T\n", e2) // *errors.errorString

	err := checkAge(-30)
	if err == nil {
		fmt.Println("ok。。。")
	} else {
		fmt.Println("失败,", err)
	}

	a, b, err := test()
	fmt.Println(a, b, err)

	fmt.Println("-------------")
	p1 := new(int)
	fmt.Printf("%T,\n", p1)
	fmt.Println(*p1)

	fmt.Println("-------------")
	p2 := new(string)
	fmt.Printf("%T\n", p2)
	fmt.Println(*p2)

}
