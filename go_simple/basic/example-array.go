package main

import "fmt"

func defineArray() {
	// 定义数组，不赋初值（使用默认值）
	var arr1 [5]int // [0 0 0 0 0]
	fmt.Println(arr1[0])
	// 定义数组，赋初值
	arr2 := [3]int{1, 2, 3}
	fmt.Println(arr2[2])
	// 定义数组，由编译器来计算长度，不可写成[]，不带长度或者 ... 的表示切片
	arr3 := []int{4, 5, 6, 7, 8}
	fmt.Println(arr3[2])
	arr4 := [...]int{7, 8, 9, 10}
	fmt.Println(arr4[2])
	// 创建指针数组
	arr5 := [2]*string{new(string), new(string)}
	*arr5[0] = "hello"
	*arr5[1] = "go"
	fmt.Println(*arr5[0], *arr5[1])
	// 为指定索引位置设置值
	arr6 := [3]int{1: 10} // [0,10,0]
	fmt.Println(arr6[1])
	// 二维数组
	var grid [4][5]int // [[0 0 0 0 0] [0 0 0 0 0] [0 0 0 0 0] [0 0 0 0 0]]
	fmt.Println(grid[0][2])
	// 数组拷贝，直接复制一份 arr2 给 arr6
	arr7 := arr2
	fmt.Println(arr7)
}

// 数组是值传递，这里的入参会拷贝一份数组（使用指针可以实现"引用传递"）
func loopArray(arr2 [3]int) {
	// 通用方法
	for i := 0; i < len(arr2); i++ {
		fmt.Println(arr2[i])
	}
	// 最简方法，只获取数组下标
	for i := range arr2 {
		fmt.Println(arr2[i])
	}
	// 最简方法，获取数组下标和对应的值
	for i, v := range arr2 {
		fmt.Println(i, v)
	}
	// 最简方法，只获取值，使用 _ 省略变量
	for _, v := range arr2 {
		fmt.Println(v)
	}
}

func main() {
	defineArray()
	loopArray([3]int{7, 8, 9})
}
