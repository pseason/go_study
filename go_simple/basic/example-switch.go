package main

import "fmt"

func eval(a, b int, op string) int {
	var value int
	switch op {
	case "+":
		value = a + b
	case "-":
		value = a - b
	default:
		panic("unsupport operator" + op)
	}
	return value
}

func example(value int) {
	switch value {
	case 1:
		fmt.Println("这里是1")
		fallthrough
	case 2:
		fmt.Println("这里是2")
		fallthrough
	default:
		fmt.Println("这里是default")
	}
}

func main() {
	fmt.Println(eval(1, 2, "+"))
	example(1)
}
