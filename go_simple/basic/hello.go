package main

import "fmt"

func hello() {
	fmt.Println("Hello world goroutine")
}

func main() {
	defer hello()
	fmt.Println("hello world")
}
