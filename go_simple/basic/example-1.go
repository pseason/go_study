package main

import (
	"fmt"
	"math"
)

// 定义包内变量
var (
	a = 1
	b = "go"
)

// 定义变量，只使用默认初值
func variableZeroValue() {
	var a int    // 0
	var s string // ""
	var b bool   // false
	fmt.Println(a, s, b)
}

// 定义变量，赋初值
func variableInitialValue() {
	var a, b = 1, 2
	b = 3
	var s = "ha"
	fmt.Println(a, b, s)
}

// 类型推断
func variableTypeDeduction() {
	var a, b, c, d = 1, 2, true, "hi"
	fmt.Println(a, b, c, d)
}

// 最简定义变量方式
func variableShorter() {
	e := 8
	a, b, c, d := 3, 2, true, "hi"
	fmt.Println(a, b, c, d, e)
}

// 常量
func consts() {
	// 指定类型
	const filename string = "filename-const"
	// 不指定类型，表示类型不定
	const a, b = 3, 4
	var c int
	// 由于类型不定，所以这里不需要强转，如果定义为 const a, b int = 3, 4，则需要强转
	c = int(math.Sqrt(a*a + b*b))
	fmt.Println(filename, a, b, c)
}

// 枚举
func enums() {
	// 使用 const 块来实现枚举
	const (
		java = 0
		cpp  = 1
		c    = 2
	)
	fmt.Println(java, cpp, c) // 0 1 2
	// 使用 iota 块来实现自增枚举
	const (
		java1 = iota
		cpp1
		c1
	)
	fmt.Println(java1, cpp1, c1) // 0 1 2
}

func main() {
	fmt.Println("go starting")
	variableZeroValue()
	variableInitialValue()
	variableTypeDeduction()
	variableShorter()
	fmt.Println(a, b)
}
