package main

import "fmt"

//值传递
func plus1(a, b int) {
	a, b = b, a
}

// 指针传递(引用传递、内存传递)
func plus2(a, b *int) {
	*a, *b = *b, *a
}

func main() {
	// 值传递
	a, b := 3, 4
	plus1(a, b)
	fmt.Println(a, b)
	// 指针传递
	plus2(&a, &b)
	fmt.Println(a, b)

}
