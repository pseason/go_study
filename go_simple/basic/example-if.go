package main

import (
	"fmt"
	"io/ioutil"
)

func readFile() {
	const filename = "hello.go"
	// Go 函数可以返回两个值
	// func ReadFile(filename string) ([]byte, error)
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	} else {
		// contents 是 []byte, 用%s 可以打印出来
		fmt.Printf("%s", contents)
	}
	// if 语句外部可访问
	fmt.Printf("%s", contents)
}

func readFileShorter() {
	const filename = "hello.go"
	// Go 函数可以返回两个值
	// func ReadFile(filename string) ([]byte, error)
	if contents, err := ioutil.ReadFile(filename); err != nil {
		fmt.Println(err)
	} else {
		// contents 是 []byte, 用%s 可以打印出来
		fmt.Printf("%s", contents)
	}
	// if 语句外部不可访问
	//fmt.Printf("%s", contents) // 报错
}
