package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	var buf [512]byte

	service := ":5000"

	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	defer conn.Close()
	checkError(err)

	rAddr := conn.RemoteAddr()

	n, err := conn.Write([]byte("hello server!"))

	checkError(err)

	n, err = conn.Read(buf[0:])

	checkError(err)

	fmt.Println("Reply from server ", rAddr.String(), string(buf[0:n]))

	os.Exit(0)
}

func checkError(err error) {
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
