package main

import "fmt"

// 多个返回值
func div(a, b int) (int, int) {
	return a / b, a % b
}

// 函数参数可以是func
func apply(op func(int, int) int, a, b int) int {
	return op(a, b)
}

// 函数参数可以是func
func apply1(op func(int, int) (int, int), a, b int) (int, int) {
	return op(a, b)
}

// 多个参数
func plus(nums ...int) int {
	s := 0
	for i := range nums {
		s = s + nums[i]
	}
	return s
}

func main() {
	q, _ := div(10, 3)
	fmt.Println(q)

	r := apply(func(x int, y int) int {
		return x + y
	}, 10, 4)
	fmt.Println(r)

	x, y := apply1(func(x int, y int) (int, int) {
		return x + y, x - y
	}, 10, 10)
	fmt.Println(x, y)

	fmt.Println(plus(1, 2, 3, 4, 5))
}
