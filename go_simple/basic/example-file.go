package main

import (
	"fmt"
	"io/ioutil"
)

func readFileA() {
	const filename string = "D:/go_study/go_simple/123.txt"
	bytes, e := ioutil.ReadFile(filename)
	if e != nil {
		fmt.Println(e)
	} else {
		fmt.Printf("%s", bytes)
		fmt.Println()
	}
	fmt.Printf("%s", bytes)
	fmt.Println()
}

func readFileB() {
	const filename = "D:/go_study/go_simple/123.txt"
	if contents, err := ioutil.ReadFile(filename); err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%s", contents)
	}
}

func main() {
	readFileA()
	fmt.Println("---------")
	readFileB()
}
