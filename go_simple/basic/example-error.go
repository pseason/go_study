package main

import (
	"fmt"
	"os"
)

type PathError struct {
	Op   string
	Path string
	Err  error
}

func (e *PathError) error() string {
	return e.Op + " " + e.Path + ": " + e.Err.Error()
}

func main() {
	f, err := os.Open("88.txt")
	if err, ok := err.(*os.PathError); ok {
		fmt.Println("File at path", err.Path, "failed to open")
		return
	}
	fmt.Println(f.Name(), "open success")
}
